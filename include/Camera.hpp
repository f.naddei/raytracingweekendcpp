#ifndef CAMERAHPP
#define CAMERAHPP

#include "Ray.hpp"
#include "MathUtilities.hpp"

class Camera
{
public:
  explicit Camera(const Vec3Float &origin, const Vec3Float &lookAt, const Vec3Float &cameraUp, double VFOV,
    double aspectRatio, double aperture, double focusDistance)
  : _origin(origin)
  {
    const double viewportHeight = 2.0 * std::tan(mathHelpers::DegreesToRadians(VFOV) / 2.);
    const double viewportWidth = viewportHeight * aspectRatio;

    _w = ComputeUnitVector(origin - lookAt);
    _u = ComputeUnitVector(cameraUp.Cross(_w));
    _v = _w.Cross(_u);

    _horizontal = focusDistance * viewportWidth * _u;
    _vertical = focusDistance * viewportHeight * _v;
    _lowerLeftCorner = _origin - _horizontal / 2.0 - _vertical / 2.0 - focusDistance * _w;

    _lensRadius = aperture / 2.;
  }

  Ray GetRay(double s, double t) const
  {
    const auto randomDiscOffset = _lensRadius * RandomInUnitDisc();
    const auto rayOrigin = _origin + randomDiscOffset.x() * _u + randomDiscOffset.y() * _v;
    return Ray(rayOrigin, _lowerLeftCorner + s * _horizontal + t * _vertical - rayOrigin);
  }

private:
  Vec3Float _origin;
  Vec3Float _horizontal;
  Vec3Float _vertical;
  Vec3Float _lowerLeftCorner;
  Vec3Float _u, _v, _w;
  double _lensRadius;
};

#endif