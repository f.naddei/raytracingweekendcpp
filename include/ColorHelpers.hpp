#ifndef COLORHELPERSHPP
#define COLORHELPERSHPP

#include <iostream>
#include <cassert>
#include <algorithm>

#include "Vec3.hpp"

void WriteColor(std::ostream &output, Color pixelColor, int samplesPerPixel)
{
  pixelColor /= static_cast<double>(samplesPerPixel);
  // Write the translated [0,255] value of each color component.
  // also use the squared root of each channel corresponding to gamma correction with gamma = 2
  const auto r = static_cast<int>(255 * std::sqrt(std::clamp(pixelColor.x(), 0., 1.)));
  const auto g = static_cast<int>(255 * std::sqrt(std::clamp(pixelColor.y(), 0., 1.)));
  const auto b = static_cast<int>(255 * std::sqrt(std::clamp(pixelColor.z(), 0., 1.)));
  output << r << ' ' << g << ' ' << b << "\n";
}

#endif