#ifndef HITTABLEHPP
#define HITTABLEHPP

#include <memory>

#include "Ray.hpp"

struct MaterialBase;

struct HitRecord
{
  Vec3Float point;
  Vec3Float normal;
  double rayLengthMult;
  bool isOnFrontFace;
  std::shared_ptr<MaterialBase> materialPtr;

  // TODO: isOnFrontFace and normal should not be defined independently
  void SetFaceNormal(const Ray &ray, const Vec3Float &outwardNormal)
  {
    isOnFrontFace = outwardNormal.Dot(ray.direction()) < 0.;
    normal = isOnFrontFace ? outwardNormal : - outwardNormal;
  }
};

class Hittable
{
public:
  Hittable(const std::shared_ptr<MaterialBase> &materialPtr) : _materialPtr(materialPtr) { }

  virtual bool Hit(const Ray &/*ray*/, double /*tMin*/, double /*tMax*/, HitRecord &/*record*/) const = 0;

  virtual ~Hittable() = default;

protected:
  std::shared_ptr<MaterialBase> _materialPtr;
};

#endif