#ifndef HITTABLELISTHPP
#define HITTABLELISTHPP

#include <memory>
#include <vector>

#include "Hittable.hpp"

class HittableList
{
public:
  HittableList() { }

  HittableList &clear()
  {
    _objects.clear();
    return *this;
  }

  HittableList &Add(std::shared_ptr<Hittable> object)
  {
    _objects.push_back(std::move(object));
    return *this;
  }

  bool Hit(const Ray &/*ray*/, double /*tMin*/, double /*tMax*/, HitRecord &/*record*/) const;

private:
  std::vector<std::shared_ptr<Hittable>> _objects;
};

#endif