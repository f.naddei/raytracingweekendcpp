#ifndef MATERIALHPP
#define MATERIALHPP

#include <algorithm>
#include <cassert>

#include "MathUtilities.hpp"
#include "Ray.hpp"

struct HitRecord;

namespace LightInteraction
{
  inline Vec3Float Reflect(const Vec3Float &v, const Vec3Float &normal)
  {
    assert(std::abs(normal.SquaredNorm() - 1.) < 1.e-14 && "Assumes that normal is normalized");
    return v - 2. * v.Dot(normal) * normal;
  }

  inline double Reflectance (double cosine, double refractionIndexRatio)
  {
    // Use Schlick's appproximation for reflectance
    auto r0 = (1. - refractionIndexRatio) / (1. + refractionIndexRatio);
    r0 *= r0;
    return r0 + (1. - r0) * std::pow((1. - cosine), 5);
  }

  inline Vec3Float Refract(const Vec3Float &v, const Vec3Float &normal, double refractIndexRatio)
  {
    assert(std::abs(v.SquaredNorm() - 1.) < 1.e-14 && "Assumes that v is normalized");
    assert(std::abs(normal.SquaredNorm() - 1.) < 1.e-14 && "Assumes that normal is normalized");
    const auto cosTheta = - v.Dot(normal);
    const auto refractionPossible = (std::sqrt(1. - cosTheta * cosTheta) * refractIndexRatio) <= 1.;
    if (!refractionPossible || Reflectance(cosTheta, refractIndexRatio) > mathHelpers::Random01())
      return Reflect(v, normal);
    else
    {
      const auto refractedNorm = refractIndexRatio * (v + cosTheta * normal);
      const auto refractedPar = - std::sqrt(std::abs(1.0 - refractedNorm.SquaredNorm())) * normal;
      return refractedNorm + refractedPar;
    }
  }
} // end namespace LightInteraction

class MaterialBase
{
public:
  virtual bool Scatter(const Ray &/*ray*/, const HitRecord &/*record*/, Color &/*attenuation*/, Ray &/*scatteredRay*/)
    const = 0;

  virtual ~MaterialBase() = default;
};

class Lambertian : public MaterialBase
{
public:
  Lambertian(const Color &color) : _albedo(color) { }

  bool Scatter(const Ray &/*ray*/, const HitRecord &record, Color &attenuation, Ray &scatteredRay) const override
  {
    scatteredRay = Ray(record.point, RandomInHemiSphere(record.normal));
    attenuation = _albedo;
    return true;
  }

private:
  Color _albedo;
};

class Metal : public MaterialBase
{
public:
  Metal(const Color &color, double fuzzyness = 0.)
  : _albedo(color), _fuzzyness(std::clamp(fuzzyness, 0., 1.))
  { }

  bool Scatter(const Ray &ray, const HitRecord &record, Color &attenuation, Ray &scatteredRay) const override
  {
    // TODO: current fuzzyness is implemented following the book, probably a better approach would use
    // RandomInHemiSphere
    const auto reflectedDir = LightInteraction::Reflect(ray.direction(), record.normal);
    const auto perturbedScatterDirection = reflectedDir / reflectedDir.Norm() + _fuzzyness * RandomInUnitSphere();
    if (perturbedScatterDirection.Dot(record.normal) > 1.e-6)
    {
      scatteredRay = Ray(record.point, perturbedScatterDirection);
      attenuation = _albedo;
      return true;
    }
    else
      return false;
  }

private:
  Color _albedo;
  double _fuzzyness;
};

class Dielectric : public MaterialBase
{
public:
  Dielectric(double refractionIndex) : _refractionIndex(refractionIndex) { }

  bool Scatter(const Ray &ray, const HitRecord &record, Color &attenuation, Ray &scatteredRay) const override
  {
    attenuation = Color::Ones();
    double reflectionRatio = record.isOnFrontFace ? (1. / _refractionIndex) : _refractionIndex;
    scatteredRay = Ray(record.point, LightInteraction::Refract(ray.direction(), record.normal, reflectionRatio));
    return true;
  }

private:
  double _refractionIndex;
};

#endif
