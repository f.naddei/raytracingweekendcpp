#ifndef MATHUTILITIESHPP
#define MATHUTILITIESHPP

#include <random>

namespace mathHelpers
{

const double pi = 3.1415926535897932385;

inline double DegreesToRadians(double degrees) {
  return degrees * pi / 180.0;
}

inline double Random01() {
  static std::uniform_real_distribution<double> distribution(0.0, 1.0);
  static std::mt19937 generator;
  return distribution(generator);
}

inline double RandomMinMax(double min, double max)
{
  return min + Random01() * (max - min);
}

}
#endif