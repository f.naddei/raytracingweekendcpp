#ifndef RAYHPP
#define RAYHPP

#include "Vec3.hpp"

class Ray
{
public:
  Ray() { }

  explicit Ray(const Vec3Float &origin, const Vec3Float &direction)
  : _origin(origin), _direction(ComputeUnitVector(direction))
  { }

  const Vec3Float &origin() const { return _origin; }

  const Vec3Float &direction() const { return _direction; }

  Vec3Float At(double t) const { return _origin + t * _direction; }

private:
  Vec3Float _origin;

  Vec3Float _direction; // warning: direction might be not normalized
};

#endif