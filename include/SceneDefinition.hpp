#ifndef SCENEDEFINITIONSHPP
#define SCENEDEFINITIONSHPP

#include "HittableList.hpp"

enum class SceneID
{
  MetalDemo,
  DielectricDemo,
  HollowDielectricDemo,
  CameraVFOVDemo,
  RandomFinal
};

HittableList CreateScene(SceneID id);

#endif