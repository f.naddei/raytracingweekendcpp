#ifndef SPHEREHPP
#define SPHEREHPP

#include "Hittable.hpp"

class Sphere: public Hittable
{
public:
  enum class Reverse : bool { True = true, False = false};

  Sphere(const Vec3Float &center, double radius, std::shared_ptr<MaterialBase> material,
    Reverse isReverse = Reverse::False)
  : Hittable(material), _center(center), _radius(radius), _reverseNormalFactor(isReverse == Reverse::False ? 1. : -1.)
  { }

  virtual bool Hit(const Ray &/*ray*/, double /*tMin*/, double /*tMax*/, HitRecord &/*record*/) const override;

private:
  const Vec3Float _center;
  const double _radius;
  const double _reverseNormalFactor;
};

#endif
