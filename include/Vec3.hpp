#ifndef VEC3HPP
#define VEC3HPP

#include <array>
#include <cmath>
#include <iostream>

#include "MathUtilities.hpp"

class Vec3Float
{
public:
  static Vec3Float Random()
  {
    return Vec3Float(mathHelpers::Random01(), mathHelpers::Random01(), mathHelpers::Random01());
  }

  static Vec3Float Random(double min, double max)
  {
    return Vec3Float(mathHelpers::RandomMinMax(min, max), mathHelpers::RandomMinMax(min, max),
      mathHelpers::RandomMinMax(min, max));
  }

  static Vec3Float Zero() { return Vec3Float(0., 0., 0.); }

  static Vec3Float Ones() { return Vec3Float(1., 1., 1.); }

  Vec3Float() : _data({0., 0., 0.}) { }

  Vec3Float(double a0, double a1, double a2) : _data({a0, a1, a2}) { }

  double x() const { return _data[0]; }
  double y() const { return _data[1]; }
  double z() const { return _data[2]; }

  Vec3Float operator-() const { return Vec3Float(-_data[0], -_data[1], -_data[2]); }

  double operator[](int i) const { return _data[i]; }
  double &operator[](int i) { return _data[i]; }

  Vec3Float &operator+=(const Vec3Float &other)
  {
    _data[0] += other._data[0];
    _data[1] += other._data[1];
    _data[2] += other._data[2];
    return *this;
  }

  Vec3Float operator+(const Vec3Float &other) const
  {
   return Vec3Float(_data[0] + other._data[0], _data[1] + other._data[1], _data[2] + other._data[2]);
  }

  Vec3Float operator-(const Vec3Float &other) const
  {
   return Vec3Float(_data[0] - other._data[0], _data[1] - other._data[1], _data[2] - other._data[2]);
  }

  Vec3Float operator*(double factor) const
  {
   return Vec3Float(factor * _data[0], factor * _data[1], factor * _data[2]);
  }

  Vec3Float &operator*=(const double factor)
  {
    _data[0] *= factor;
    _data[1] *= factor;
    _data[2] *= factor;
    return *this;
  }

  Vec3Float ElementwiseProduct(const Vec3Float &other) const
  {
    return Vec3Float(_data[0] * other._data[0], _data[1] * other._data[1], _data[2] * other._data[2]);
  }

  Vec3Float operator/(double factor) const { return *this * (1. / factor); }

  Vec3Float &operator/=(const double factor){ return *this *= 1. / factor; }

  double Dot(const Vec3Float &other) const
  {
    return _data[0] * other._data[0] + _data[1] * other._data[1] + _data[2] * other._data[2];
  }

  Vec3Float Cross(const Vec3Float &other) const
  {
    return Vec3Float(_data[1] * other._data[2] - _data[2] * other._data[1],
      _data[2] * other._data[0] - _data[0] * other._data[2], _data[0] * other._data[1] - _data[1] * other._data[0]);
  }

  double SquaredNorm() const { return this->Dot(*this); }

  double Norm() const { return std::sqrt(this->SquaredNorm()); }

  friend std::ostream& operator<<(std::ostream &output, const Vec3Float& v)
  {
    output << "[" << v._data[0] << " " << v._data[1] << " " << v._data[2] << "]";
    return output;
  }

  double MaxElement() const { return std::max(_data[0], std::max(_data[1], _data[2])); }

private:
  std::array<double, 3> _data;
};

inline Vec3Float operator*(double factor, const Vec3Float& v) { return v * factor; }

inline Vec3Float ComputeUnitVector(const Vec3Float &v) { return v / v.Norm(); }

inline Vec3Float ComputeUnitVector(Vec3Float &&v) {
  v /= v.Norm();
  return std::move(v);
}

inline Vec3Float RandomInUnitSphere()
{ /// TODO: use smarter implementation
  while (true)
  {
    const auto v = Vec3Float::Random(-1., 1.);
    if (v.SquaredNorm() < 1.)
      return v;
  }
}

inline Vec3Float RandomInUnitDisc()
{ /// TODO: use smarter implementation
  auto randGen = [](){ return mathHelpers::RandomMinMax(-1, 1.); };
  while (true)
  {
    const auto v = Vec3Float(randGen(), randGen(), 0.);
    if (v.SquaredNorm() < 1.)
      return v;
  }
}

inline Vec3Float RandomInHemiSphere(const Vec3Float &base)
{
  auto inUnitSphere = RandomInUnitSphere();
  if (inUnitSphere.Dot(base) > 0.) // in the same hemisphere as the base vector
    return inUnitSphere;
  else
    return - inUnitSphere;
}

using Color = Vec3Float;
using Point3 = Vec3Float;

#endif