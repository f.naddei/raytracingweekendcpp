#include <iostream>
#include <limits>
#include <chrono>

#include "MathUtilities.hpp"
#include "Camera.hpp"
#include "Vec3.hpp"
#include "ColorHelpers.hpp"
#include "Ray.hpp"
#include "HittableList.hpp"
#include "Materials.hpp"
#include "SceneDefinition.hpp"

Color RayColor(const Ray &ray, const HittableList &world, int nMaxReflections, const Color &accumulated = Color::Ones())
{
  static constexpr double thres = 1. / 256.;
  if (nMaxReflections < 1 || accumulated.MaxElement() < thres)
    return Color::Zero();

  auto hitRecord = HitRecord();
  if (world.Hit(ray, 0.00001/*avoid self reflection*/, std::numeric_limits<double>::infinity(), hitRecord))
  {
    Ray reflectedRay;
    Color attenuation;
    if (hitRecord.materialPtr->Scatter(ray, hitRecord, attenuation, reflectedRay))
      return RayColor(reflectedRay, world, nMaxReflections - 1, attenuation.ElementwiseProduct(accumulated));
    else
      return Color::Zero();
  }

  const auto &direction = ray.direction();
  const auto t = 0.5 * (direction.y() + 1.);
  return accumulated.ElementwiseProduct((1. - t) * Color::Ones() + t * Color(0.5, 0.7, 1.));
}

std::vector<Vec3Float> ComputeAllPixelColors(const Camera &camera, const HittableList &world, int imageWidth,
  int imageHeight, int samplesPerPixel, bool printProgress = true)
{
  const int nMaxReflections = 50;

  std::vector<Color> pixelColors(imageWidth * imageHeight);
  for (auto j = imageHeight - 1; j >= 0; --j)
  {
    if (printProgress)
      std::cerr << "\rProgress: " << std::round(static_cast<double>(imageHeight - j) / imageHeight * 100) << " % "
        << std::flush;
    for (auto i = decltype(imageWidth){0}; i < imageWidth; ++i)
    {
      auto &pixelColor = pixelColors[(imageHeight - j - 1) * imageWidth + i];
      pixelColor = Color::Zero();
      for (int sample = 0; sample < samplesPerPixel; ++sample)
      {
        const auto u = (static_cast<double>(i) + mathHelpers::Random01()) / (imageWidth - 1);
        const auto v = (static_cast<double>(j) + mathHelpers::Random01()) / (imageHeight - 1);
        const auto r = camera.GetRay(u, v);
        pixelColor += RayColor(r, world, nMaxReflections);
      }
    }
  }
  return pixelColors;
}

int main()
{
  const auto world = CreateScene(SceneID::RandomFinal);

  // Image
  const double aspectRatio{16. / 9.};
  const int imageHeight{300};
  const int imageWidth = imageHeight * aspectRatio;
  const int samplesPerPixel = 200;

  // Camera
  const auto cameraOrigin = Vec3Float(13., 2., 3.);
  const auto cameraLookAt = Vec3Float(0., 0., 0.);
  const auto distanceToFocus = 10.0;
  const auto camera = Camera(cameraOrigin, cameraLookAt, Vec3Float(0., 1., 0.), 20. /*VFOV*/, aspectRatio,
    0.1 /*aperture*/, distanceToFocus);

  // Render
  const auto timerStart = std::chrono::steady_clock::now();
  const auto pixelColors = ComputeAllPixelColors(camera, world, imageWidth, imageHeight, samplesPerPixel);
  const auto timerEnd = std::chrono::steady_clock::now();
  const std::chrono::duration<double> elapsedSeconds = timerEnd - timerStart;
  std::cerr << "\rDone in : " << elapsedSeconds.count() << "s\n";

  std::cerr << "Writing ...";
  std::cout << "P3\n" << imageWidth << " " << imageHeight << "\n255\n";
  for (const auto &pixelColor : pixelColors)
    WriteColor(std::cout, pixelColor, samplesPerPixel);
  std::cerr << "\r";
}
