## Raytracing in a weekend implementation in C++

Implementation of a simple raytracer based on the book
[raytracing in a weekend](https://raytracing.github.io/books/RayTracingInOneWeekend.html).

<div align="center">

  <img src="out.png" width="600">
</div>

### Requirements
* CMake with version 3.10 or higher
* A C++ compiler supporting the C++17 standard or higher

### Installation and usage
Compiling:

```
mkdir build
cd build
cmake ..
cmake --build .
```

The above will produce an executable named *raytrace*.

The executable produces the final example of the book directly to the standard output therefore to save the image to
a file named "output.ppm" simply use:

```
./raytrace > output.ppm
```
