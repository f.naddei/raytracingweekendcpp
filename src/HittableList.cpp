#include "HittableList.hpp"

bool HittableList::Hit(const Ray &ray, double tMin, double tMax, HitRecord &record) const
{
  bool hitAnything = false;
  auto closest_so_far = tMax;

  for (const auto &object : _objects)
    if (object->Hit(ray, tMin, closest_so_far, record))
    {
      hitAnything = true;
      closest_so_far = record.rayLengthMult;
    }

  return hitAnything;
}