#include "SceneDefinition.hpp"

#include "Materials.hpp"
#include "Sphere.hpp"

HittableList CreateScene(SceneID id)
{
  HittableList world;
  switch (id)
  {
    case SceneID::MetalDemo:
    {
      const auto groundMaterial = std::make_shared<Lambertian>(Color(0.8, 0.8, 0.));
      const auto centerSphereMaterial = std::make_shared<Lambertian>(Color(0.7, 0.3, 0.3));
      const auto leftSphereMaterial = std::make_shared<Metal>(Color(0.8, 0.8, 0.8), 0.3);
      const auto rightSphereMaterial = std::make_shared<Metal>(Color(0.8, 0.6, 0.3), 1.0);

      world.Add(std::make_shared<Sphere>(Vec3Float(0., -100.5, -1.), 100., groundMaterial))
        .Add(std::make_shared<Sphere>(Vec3Float(0., 0., -1.), 0.5, centerSphereMaterial))
        .Add(std::make_shared<Sphere>(Vec3Float(-1., 0., -1.), 0.5, leftSphereMaterial))
        .Add(std::make_shared<Sphere>(Vec3Float(1., 0., -1.), 0.5, rightSphereMaterial));
      break;
    }

    case SceneID::DielectricDemo:
    {
      const auto groundMaterial = std::make_shared<Lambertian>(Color(0.8, 0.8, 0.));
      const auto centerSphereMaterial = std::make_shared<Lambertian>(Color(0.1, 0.2, 0.5));
      const auto leftSphereMaterial = std::make_shared<Dielectric>(1.5);
      const auto rightSphereMaterial = std::make_shared<Metal>(Color(0.8, 0.6, 0.3), 0.0);

      world.Add(std::make_shared<Sphere>(Vec3Float(0., -100.5, -1.), 100., groundMaterial))
        .Add(std::make_shared<Sphere>(Vec3Float(0., 0., -1.), 0.5, centerSphereMaterial))
        .Add(std::make_shared<Sphere>(Vec3Float(-1., 0., -1.), 0.5, leftSphereMaterial))
        .Add(std::make_shared<Sphere>(Vec3Float(1., 0., -1.), 0.5, rightSphereMaterial));
      break;
    }

    case SceneID::HollowDielectricDemo:
    {
      const auto groundMaterial = std::make_shared<Lambertian>(Color(0.8, 0.8, 0.));
      const auto centerSphereMaterial = std::make_shared<Lambertian>(Color(0.1, 0.2, 0.5));
      const auto leftSphereMaterial = std::make_shared<Dielectric>(1.5);
      const auto rightSphereMaterial = std::make_shared<Metal>(Color(0.8, 0.6, 0.3), 0.0);

      world.Add(std::make_shared<Sphere>(Vec3Float(0., -100.5, -1.), 100., groundMaterial))
        .Add(std::make_shared<Sphere>(Vec3Float(0., 0., -1.), 0.5, centerSphereMaterial))
        .Add(std::make_shared<Sphere>(Vec3Float(-1., 0., -1.), 0.5, leftSphereMaterial))
        .Add(std::make_shared<Sphere>(Vec3Float(-1., 0., -1.), 0.4, leftSphereMaterial, Sphere::Reverse::True))
        .Add(std::make_shared<Sphere>(Vec3Float(1., 0., -1.), 0.5, rightSphereMaterial));
      break;
    }

    case SceneID::CameraVFOVDemo:
    {
      const auto R = std::cos(mathHelpers::pi / 4.);
      const auto materialLeft  = std::make_shared<Lambertian>(Color(0., 0., 1.));
      const auto materialRight = std::make_shared<Lambertian>(Color(1., 0., 0.));

      world.Add(std::make_shared<Sphere>(Vec3Float(-R, 0., -1.), R, materialLeft))
        .Add(std::make_shared<Sphere>(Vec3Float(R, 0., -1), R, materialRight));
      break;
    }

    case SceneID::RandomFinal:
    {
      using mathHelpers::Random01;
      using mathHelpers::RandomMinMax;

      const auto groundMaterial = std::make_shared<Lambertian>(Color(0.5, 0.5, 0.5));
      world.Add(std::make_shared<Sphere>(Vec3Float(0., -10000., 0.), 10000., groundMaterial))
        .Add(std::make_shared<Sphere>(Vec3Float(0., 1., 0.), 1., std::make_shared<Dielectric>(1.5)))
        .Add(std::make_shared<Sphere>(Vec3Float(-4., 1., 0.), 1., std::make_shared<Lambertian>(Color(0.4, 0.2, 0.1))))
        .Add(std::make_shared<Sphere>(Vec3Float(4., 1., 0.), 1.0, std::make_shared<Metal>(Color(0.7, 0.6, 0.5), 0.0)));

      std::vector<Vec3Float> centers;
      for (int a = -11; a < 11; a++)
        for (int b = -11; b < 11; b++)
        {
          while(true)
          {
            Vec3Float candidate(a + 0.9 * Random01(), 0.2, b + 0.9 * Random01());
            bool collides = false;
            for (const auto &center : centers)
            {
              if ((center - candidate).Norm() < 0.4)
              {
                collides = true;
                break;
              }
            }
            if (!collides)
            {
              centers.push_back(candidate);
              break;
            }
          }
        }

      for (const auto &center : centers)
      {
        if ((center - Vec3Float(4., 1., 0.)).Norm() < 1.2 || (center - Vec3Float(0., 1., 0.)).Norm() < 1.2 ||
          (center - Vec3Float(-4., 1., 0.)).Norm() < 1.2)
          continue;

        const auto chooseMaterial = Random01();
        if (chooseMaterial < 0.8)
        { // diffuse
          Color colorRandom;
          const auto a = RandomMinMax(0.6, 1.);
          if (Random01() > 0.3)
            colorRandom = Color(a, 0.4 * a * Random01(), 0.4 * a * Random01());
          else
            colorRandom = Color(0.4 * a * Random01(), 0.4 * a * Random01(), a);
          world.Add(std::make_shared<Sphere>(center, 0.2, std::make_shared<Lambertian>(colorRandom)));
        }
        else if (chooseMaterial < 0.95)
        { // metal
          world.Add(std::make_shared<Sphere>(center, 0.2,
            std::make_shared<Metal>(Color::Random(0.5, 1), RandomMinMax(0., 0.5))));
        }
        else
        { // glass
          world.Add(std::make_shared<Sphere>(center, 0.2, std::make_shared<Dielectric>(1.5)));
        }
      }

      break;
    }

    default:
      throw;
  }
  return world;
}