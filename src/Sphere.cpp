#include "Sphere.hpp"

#include <cassert>

#include "Vec3.hpp"

bool Sphere::Hit(const Ray &ray, double tMin, double tMax, HitRecord &record) const
{
  assert(std::abs(ray.direction().SquaredNorm() - 1.) < 1.e-14);
  const auto oc = ray.origin() - _center;
  const auto ocSquaredNorm = oc.SquaredNorm();
  if (ocSquaredNorm > ((tMax + _radius) * (tMax + _radius)))
    return false;

  const auto halfB = oc.Dot(ray.direction());
  const auto c = ocSquaredNorm - _radius * _radius;
  const auto discriminant = halfB * halfB - c;
  if (discriminant < 0.)
    return false;
  else
  {
    const auto sqrtDiscriminant = std::sqrt(discriminant);
    auto t = (-halfB - sqrtDiscriminant); // try first solution
    if (t < tMin || t > tMax)
    {
      t = (-halfB + sqrtDiscriminant); // try second solution
      if (t < tMin || t > tMax)
        return false;
    }

    // a valid intersection has been found
    record.point = ray.At(t);

    const auto outwardNormal = _reverseNormalFactor * ComputeUnitVector(record.point - _center);
    // dividing by the radius would also give a unit vector but is slightly less precise
    // The book uses a trick where dividing by radius and having a negative radius allows mimicking the effect of an
    // "inside-out" sphere which combined with another sphere can be used to simulate an hollow sphere. This trick needs
    // needs to be done explicitly by using _reverseNormalFactor as we do not us the radius directly anymore.

    record.rayLengthMult = t;
    record.SetFaceNormal(ray, outwardNormal);
    record.materialPtr = this->_materialPtr;
    return true;

  }
}